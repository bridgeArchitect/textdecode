package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"unicode/utf8"
)

const (
	filenameRes = "result.txt"
	a = int32(20)
	b = int32(32)
	c = int32(33)
)

/* function to handle error */
func handleError(err error) {

	/* handle error */
	if err != nil {
		log.Fatal(err)
	}

}

/* decode text */
func decodeText(filename string) {

	/* declaration of variables */
	var (
		err        error
		fileInput  *os.File
		fileOutput *os.File
		line       []byte
		row        string
		newRow     string
		width, w   int
		i          int
		symbol     int32
		newSymbol  int32
		hashTable  map[int32]int32
		hashTableR map[int32]int32
	)

	/* open file for reading */
	fileInput, err = os.Open(filename)
	handleError(err)

	/* create file to write results */
	fileOutput, err = os.Create(filenameRes)
	handleError(err)

	/* create numeration of symbols */
	hashTable = make(map[int32]int32)
	hashTable['а'] = 0
	hashTable['б'] = 1
	hashTable['в'] = 2
	hashTable['г'] = 3
	hashTable['ґ'] = 4
	hashTable['д'] = 5
	hashTable['е'] = 6
	hashTable['є'] = 7
	hashTable['ж'] = 8
	hashTable['з'] = 9
	hashTable['и'] = 10
	hashTable['і'] = 11
	hashTable['ї'] = 12
	hashTable['й'] = 13
	hashTable['к'] = 14
	hashTable['л'] = 15
	hashTable['м'] = 16
	hashTable['н'] = 17
	hashTable['о'] = 18
	hashTable['п'] = 19
	hashTable['р'] = 20
	hashTable['с'] = 21
	hashTable['т'] = 22
	hashTable['у'] = 23
	hashTable['ф'] = 24
	hashTable['х'] = 25
	hashTable['ц'] = 26
	hashTable['ч'] = 27
	hashTable['ш'] = 28
	hashTable['щ'] = 29
	hashTable['ь'] = 30
	hashTable['ю'] = 31
	hashTable['я'] = 32

	/* create reversed numeration of symbols */
	hashTableR = make(map[int32]int32)
	hashTableR[0] = 'a'
	hashTableR[1] = 'б'
	hashTableR[2] = 'в'
	hashTableR[3] = 'г'
	hashTableR[4] = 'ґ'
	hashTableR[5] = 'д'
	hashTableR[6] = 'e'
	hashTableR[7] = 'є'
	hashTableR[8] = 'ж'
	hashTableR[9] = 'з'
	hashTableR[10] = 'и'
	hashTableR[11] = 'і'
	hashTableR[12] = 'ї'
	hashTableR[13] = 'й'
	hashTableR[14] = 'к'
	hashTableR[15] = 'л'
	hashTableR[16] = 'м'
	hashTableR[17] = 'н'
	hashTableR[18] = 'о'
	hashTableR[19] = 'п'
	hashTableR[20] = 'р'
	hashTableR[21] = 'с'
	hashTableR[22] = 'т'
	hashTableR[23] = 'у'
	hashTableR[24] = 'ф'
	hashTableR[25] = 'x'
	hashTableR[26] = 'ц'
	hashTableR[27] = 'ч'
	hashTableR[28] = 'ш'
	hashTableR[29] = 'щ'
	hashTableR[30] = 'ь'
	hashTableR[31] = 'ю'
	hashTableR[32] = 'я'

	/* read all file */
	line, err = ioutil.ReadAll(fileInput)
	handleError(err)

	/* convert to string */
	row = string(line)
	newRow = ""
	/* handle row */
	w = 0
	for i = 0; i < len(row); i += w {
		/* handle the symbol (encode) */
		symbol, width = utf8.DecodeRuneInString(row[i:])
		/* find symbol in hash table and apply decode rule */
		if _, ok := hashTable[symbol]; ok {
			newSymbol = hashTable[symbol]
			newSymbol = (a * newSymbol - a * b) % c
			if newSymbol < 0 {
				newSymbol = 32 + newSymbol
			}
			newRow = newRow + string(hashTableR[newSymbol])
		}
		w = width
	}
	fmt.Println()

	/* write final result */
	_, err = fileOutput.WriteString(newRow + "\n")
	handleError(err)


	/* close files */
	err = fileInput.Close()
	handleError(err)
	err = fileOutput.Close()
	handleError(err)

}

/* entry point */
func main() {

	/* declarations of variables */
	var (
		filename   string
	)

	/* read filename */
	filename = os.Args[1]
	decodeText(filename)

}